---
title: 'Pasante de investigación en procesamiento de señales'
type: 'internship'
summary: ''
startDate: '2003-05-02'
endDate: '2003-08-31'
isCurrent: false
company: 'irisa'
remote: false
languages:
  - fr
languagesAlso:
  - en
---
