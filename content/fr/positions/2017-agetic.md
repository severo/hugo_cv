---
title: "Chef de l'unité d'innovation, recherche et développement'
type: 'permanent'
summary: ''
startDate: '2017-10-15'
endDate: '2018-02-28'
isCurrent: false
company: 'agetic'
remote: false
slug: 2017-agetic
languages:
  - es
languagesAlso:
  - en
---

Supervisión de proyectos de simplificación de trámites y gobierno electrónico,
asesoramiento técnico, redacción de normativa.
