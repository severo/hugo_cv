---
title: 'Professeur du cours de récupération de développement logiciel en environnements libres'
type: 'contract'
summary: ''
startDate: '2018-06-21'
endDate: '2018-06-25'
isCurrent: false
company: uasb
remote: true
languages:
  - es
---

Ver el [soporte de cursos](https://cursos.rednegra.net/201806-UASB-desarrollo-de-software-en-entornos-libres/) en línea.
