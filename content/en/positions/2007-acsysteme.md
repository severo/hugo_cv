---
title: 'Research and development engineer'
type: 'permanent'
summary: ''
startDate: '2007-10-01'
endDate: '2009-02-28'
isCurrent: false
company: 'acsysteme'
remote: false
languages:
  - fr
languagesAlso:
  - en
---
