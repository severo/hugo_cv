---
title: 'University of Metz'
companyType: 'D'
locations:
  - id: 1
    address:
      street1: 'Campus Saulcy'
      street2: ''
      city: 'Metz'
      state: ''
      postalCode: '35000'
      countryCode: 'FR'
    contactInfo:
      phone1: ''
      phone2: ''
websiteUrl: 'http://www.univ-lorraine.fr/'
logoUrl: ''
twitterId: 'Univ_Lorraine'
description: ''
isActive: true
---
