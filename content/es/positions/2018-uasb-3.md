---
title: 'Coordinador de la Maestría en gerencia de tecnologías libres'
type: 'temporal'
summary: ''
startDate: '2018-09-01'
endDate: ''
isCurrent: true
company: uasb
remote: true
languages:
  - es
languagesAlso:
  - en
  - fr
---
