---
title: 'Doctorado en Telecomunicaciones y Procesamiento de señales'
schoolName: 'Universidad de Rennes 1'
fieldOfStudy: 'Telecomunicaciones y Procesamiento de señales'
startDate: '2003-05-01'
endDate: '2007-04-30'
degree: 'Doctorado'
activities: ''
notes: ''
school: 'universite-de-rennes1'
languages:
  - fr
languagesAlso:
  - en
---

« Aprendizaje de diccionarios estructurados para la modelización parsimoniosa de las señales multicanal »
