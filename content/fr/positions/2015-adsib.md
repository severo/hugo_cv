---
title: 'Directeur exécutif'
type: 'permanent'
summary: ''
startDate: '2015-05-01'
endDate: '2016-09-15'
isCurrent: false
company: 'adsib'
remote: false
languages:
  - es
languagesAlso:
  - en
skills:
  - project-management
  - technical-writing
  - policy-making
  - software-design
  - datacenter-design
skillsAlso:
  - linux-server-administration
  - networks-administration
---

Gestion de projets, entre autres conception et implémentation d’un datacenter, implémentation de l’Atorité de Certification Publique de Bolivie (https://firmadigital.bo), développement d’un atlas géographique historiaue de Bolivie (http://georchestra.gob.bo).
