---
title: 'Universidad Mayor de San Andrés'
companyType: 'D'
locations:
  - id: 1
    address:
      street1: 'J.J. Perez'
      street2: ''
      city: 'La Paz'
      state: ''
      postalCode: ''
      countryCode: 'BO'
    contactInfo:
      phone1: '+591 2 2612298'
      phone2: ''
websiteUrl: 'http://www.umsa.bo'
logoUrl: ''
twitterId: 'informacionumsa'
description: ''
isActive: true
---
