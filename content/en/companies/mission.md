---
title: 'Mission of Bolivia to the United Nations in Geneva'
companyType: 'G'
locations:
  - id: 1
    address:
      street1: 'Rue de Lausanne 139'
      street2: ''
      city: 'Geneva'
      state: ''
      postalCode: '1202'
      countryCode: 'CH'
    contactInfo:
      phone1: +41 22 908 0717
      ? phone2
websiteUrl: 'http://boliviaenlaonu.org/'
logoUrl: ''
twitterId: 'BoliviaONUGva'
description: ''
isActive: true
---
