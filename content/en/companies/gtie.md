---
title: 'GTIE Ardennes'
companyType: 'P'
locations:
  - id: 1
    address:
      street1: 'Rue des panses brûlées'
      street2: 'Zone artisanale'
      city: 'Vrigne-aux-bois'
      state: ''
      postalCode: '08330'
      countryCode: 'FR'
    contactInfo:
      phone1: '+33 3 29 87 68 51'
      phone2: ''
websiteUrl: 'https://www.actemium.com/'
logoUrl: ''
twitterId: 'Actemium'
description: ''
isActive: true
---
