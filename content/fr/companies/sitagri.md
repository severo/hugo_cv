---
title: 'Sitagri'
companyType: 'P'
locations:
  - id: 1
    address:
      street1: ''
      street2: ''
      city: 'Rennes'
      state: ''
      postalCode: ''
      countryCode: 'FR'
    contactInfo:
      phone1: ''
      phone2: ''
websiteUrl: 'http://www.financeagri.com'
logoUrl: ''
twitterId: 'FinanceAgri_SAS'
description: ''
isActive: true
---
