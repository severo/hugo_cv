---
title: 'PhD in Telecommunications and Signal Processing'
schoolName: 'University of Rennes I'
fieldOfStudy: 'Telecommunications and signal processing'
startDate: '2003-05-01'
endDate: '2007-04-30'
degree: 'PhD'
activities: ''
notes: ''
school: 'universite-de-rennes1'
languages:
  - fr
languagesAlso:
  - en
---
