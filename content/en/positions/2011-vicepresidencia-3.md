---
title: 'Systems and development officer of the GeoBolivia project'
type: 'temporal'
summary: ''
startDate: '2011-12-01'
endDate: '2013-01-31'
isCurrent: false
company: 'vicepresidencia'
remote: false
languages:
  - es
languagesAlso:
  - en
  - fr
---
