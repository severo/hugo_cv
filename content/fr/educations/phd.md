---
title: 'Doctorat en Télécommunications et traitement de signal'
schoolName: 'Université de Rennes 1'
fieldOfStudy: 'Télécommunications et traitement de signal'
startDate: '2003-05-01'
endDate: '2007-04-30'
degree: 'Doctorat'
activities: ''
notes: ''
school: 'universite-de-rennes1'
languages:
  - fr
languagesAlso:
  - en
---
