---
title: 'Instituto de Investigación en Informática y Sistemas Aleatorios - IRISA'
companyType: 'G'
locations:
  - id: 1
    address:
      street1: 'Campus universitaire de Beaulieu'
    - street2: '263 Avenue du Général Leclerc'
      city: 'Rennes'
      state: ''
      postalCode: '35042'
      countryCode: 'FR'
    contactInfo:
      phone1: '+33 2 99 84 71 00'
      phone2: ''
websiteUrl: 'https://www.irisa.fr'
logoUrl: ''
twitterId: 'Irisa_lab'
description: ''
isActive: true
---
