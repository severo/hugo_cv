---
title: 'Stagiaire de recherche en traitement de signal'
type: 'internship'
summary: ''
startDate: '2003-05-02'
endDate: '2003-08-31'
isCurrent: false
company: 'irisa'
remote: false
languages:
  - fr
languagesAlso:
  - en
---
