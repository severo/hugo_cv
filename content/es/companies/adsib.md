---
title: 'Agencia para el Desarrollo de la Sociedad de la Información en Bolivia - ADSIB'
universalName: 'adsib'
companyType: 'G'
locations:
  - id: 1
    address:
      street1: 'Calle Ayacucho y Mercado No 308'
      street2: 'Edificio de la Vicepresidencia del Estado Piso 3'
      city: 'La Paz'
      state: ''
      postalCode: ''
      countryCode: 'BO'
    contactInfo:
      phone1: +591 2 2200720
      phone2: +591 2 2200730
websiteUrl: 'https://adsib.gob.bo'
logoUrl: ''
twitterId: 'ADSIB1'
description: ''
isActive: true
---
