---
title: 'Andean University Simón Bolivar'
companyType: 'D'
locations:
  - id: 1
    address:
      street1: 'Calle San Salvador #1351'
      street2: ''
      city: 'La Paz'
      state: ''
      postalCode: ''
      countryCode: 'BO'
    contactInfo:
      phone1: '+591 2 2112230'
      phone2: ''
  - id: 2
    address:
      street1: 'Calle Real Audiencia #73'
      street2: ''
      city: 'La Paz'
      state: ''
      postalCode: ''
      countryCode: 'BO'
    contactInfo:
      phone1: '+591 4 6460265'
      phone2: ''
websiteUrl: 'http://www.uasb.edu.bo'
logoUrl: ''
twitterId: 'u_andina'
description: ''
isActive: true
---
