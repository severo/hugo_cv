---
title: 'Head of innovation, research and development unit'
type: 'permanent'
summary: ''
startDate: '2017-10-15'
endDate: '2018-02-28'
isCurrent: false
company: 'agetic'
remote: false
slug: 2017-agetic
languages:
  - es
languagesAlso:
  - en
---

Supervision of e-goverment projects, technical assessment, redaction of
technical norms.
