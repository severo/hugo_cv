---
title: 'Rennes I University'
companyType: "D"
locations:
  - id: 1
    address:
      street1: 'Campus de Beaulieu'
      street2: ''
      city: 'Rennes'
      state: ''
      postalCode: '35000'
      countryCode: 'FR'
    contactInfo:
      phone1: '+33 2 23 23 63 27'
      phone2: ''
websiteUrl: 'https://www.univ-rennes1.fr/'
logoUrl: ''
twitterId: 'univrennes1'
description: ''
isActive: true
---
