---
title: 'Administradora Boliviana de Carreteras - ABC'
companyType: 'G'
locations:
  - id: 1
    address:
      street1: 'Av. Mcal. Santa Cruz'
      street2: 'Edif. Centro de Comunicaciones La Paz Piso 8'
      city: 'La Paz'
      state: ''
      postalCode: ''
      countryCode: 'BO'
    contactInfo:
      phone1: +591 2 2159800
      ? phone2
websiteUrl: 'http://abc.gob.bo'
logoUrl: ''
twitterId: 'admCarreterasBo'
description: ''
isActive: true
---
