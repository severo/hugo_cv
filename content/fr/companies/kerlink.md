---
title: 'Kerlink'
companyType: 'P'
locations:
  - id: 1
    address:
      street1: '1 Rue Jacqueline Auriol'
      street2: ''
      city: 'Thorigné-Fouillard'
      state: ''
      postalCode: '35235'
      countryCode: 'FR'
    contactInfo:
      phone1: +33 2 99 12 29 00
      ? phone2
websiteUrl: 'http://www.kerlink.com'
logoUrl: ''
twitterId: 'kerlink_news'
description: ''
isActive: true
---
