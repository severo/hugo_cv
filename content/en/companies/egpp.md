---
title: 'Plurinational Public Administration School'
companyType: 'D'
locations:
  - id: 1
    address:
      street1: 'Calle Bolivar Nº724 Esq. Indaburo'
      street2: ''
      city: 'La Paz'
      state: ''
      postalCode: ''
      countryCode: 'BO'
    contactInfo:
      phone1: '+591 2 2200353'
      phone2: '+591 2 2200131'
websiteUrl: 'http://www.egpp.gob.bo'
logoUrl: ''
twitterId: 'egppbolivia'
description: ''
isActive: true
---
