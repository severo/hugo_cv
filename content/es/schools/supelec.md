---
title: 'Escuela Superior de Electricidad'
companyType: "D"
locations:
  - id: 1
    address:
      street1: 'Avenue de la Boulaie'
      street2: ''
      city: 'Rennes'
      state: ''
      postalCode: '35576'
      countryCode: 'FR'
    contactInfo:
      phone1: '+33 2 99 84 45 00'
      phone2: ''
websiteUrl: 'http://www.centralesupelec.fr/'
logoUrl: ''
twitterId: 'centralesupelec'
description: ''
isActive: true
---
