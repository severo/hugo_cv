---
title: 'Vicepresidencia del Estado Plurinacional de Bolivia'
companyType: 'G'
locations:
  - id: 1
    address:
      street1: 'Calle Ayacucho y Mercado No 308'
      street2: ''
      city: 'La Paz'
      state: ''
      postalCode: ''
      countryCode: 'BO'
    contactInfo:
      phone1: +591 2 2142000
      ? phone2
websiteUrl: 'https://vicepresidencia.gob.bo'
logoUrl: ''
twitterId: 'VPEP_Bol'
description: ''
isActive: true
---
