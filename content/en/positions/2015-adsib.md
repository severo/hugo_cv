---
title: 'Executive director'
type: 'permanent'
summary: ''
startDate: '2015-05-01'
endDate: '2016-09-15'
isCurrent: false
company: 'adsib'
remote: false
languages:
  - es
languagesAlso:
  - en
skills:
  - project-management
  - technical-writing
  - policy-making
  - software-design
  - datacenter-design
skillsAlso:
  - linux-server-administration
  - networks-administration
---
