---
title: 'Agency of Electronic Government, and Information and Communication Technologies - AGETIC'
universalName: 'ageticbolivia'
companyType: 'G'
locations:
  - id: 1
    address:
      street1: 'Calle Pedro Salazar Nº 631, esq. Andrés Muñoz'
      street2: ''
      city: 'La Paz'
      state: ''
      postalCode: ''
      countryCode: 'BO'
    contactInfo:
      phone1: +591 2 2128706
      phone2: +591 2 2128707
websiteUrl: 'https://agetic.gob.bo'
logoUrl: ''
twitterId: 'AgeticBolivia'
description: ''
isActive: true
---
